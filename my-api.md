# Git Commands

- Git Init
``` bash
$ git init 
$ git status
```


- Git Add, Commit
``` bash
$ git add .
$ git add <file-name>
$ git commit -m "message"
$ git commit -m "message" -a
```

- Git Checkout
``` bash
$ git checkout <hash>
$ git checkout <file-name> <hash>
$ git checkout <branch>
$ git checkout <branch> -a
$ git checkout <tag>
```

- Git Branch
``` bash
$ git branch 
$ git branch <new-branch-name>
$ git checkout <branch>
$ git branch -d|-D <branch-name>
```
- Git Tag
``` bash
$ git tag 
$ git tag <new-tag-name>
$ git checkout <tag>
```

- Git Status and Log
``` bash
$ git status 
$ git log
```

- Git Remote
```bash
- Remote: [origin,upstream]
$ git remote -v
$ git remote add origin/upstream <remote-address>
$ git remote remote origin/upstream 
$ git remote set-url origin/upstream <remote-address>
```

- Git Push
``` bash
$ git push 
$ git push origin <branch>
$ git push origin <tag>
```

- Git Fetch
``` bash
$ git fetch
$ git fetch <branch>
$ git fetch <tag>
```